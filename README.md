# What is this?

This is a simple proof of concept for a packet radio BBS that relies on an
external AGW server for communications, specifically Direwolf.

# How do I run this?

Clone this repository:

```
git clone git@gitlab.com:jlewallen/py-agw-bbs.git
cd py-agw-bbs
```

This project uses `venv` to manage pip dependencies, so it's necessary to activate that environment
and install those dependencies:

```
./activate.sh
```

Enable AGW in your Direwolf install, if you haven't already. You'll need the IP
address and port of the server.

Run it! Paying special attention to the AGW address and port as well the node's
callsign. Once this is running you should be able to connect to the node like
you would a BPQ instance.

```
src/main.py --agw-server 127.0.0.1:8000 --my-call MYCALL-1
```

In this case `MYCALL-1` is the callsign and SSID the BBS (stretching the word) will respond as. Once this is running you'll probably want to then connect to the BBS. This can be tricky, depending your setup. If you have BPQ connected to the same DW instance and try it via telnet, it won't work. Direwolf won't send the BBS the traffic BPQ is transmitting. This means you need another node. Or...

# Linux Test Environment

If you're running linux, you can setup a nifty little testing environment.

Load ALSA's loopback audio device:

`sudo modprobe -r snd-aloop`

Create two DireWolf configurations:

Call this one `dw-loopback-1.conf`

```
ADEVICE plughw:4,0,0 plughw:4,0,1
ACHANNELS 1
CHANNEL 0
MODEM 1200
AGWPORT 7100
KISSPORT 0
```

Call this one `dw-loopback-2.conf`

```
DEVICE plughw:4,1,1 plughw:4,1,0
ACHANNELS 1
CHANNEL 0
MODEM 1200
AGWPORT 7102
KISSPORT 0
```

Verify the `DEVICE` lines above. The module should have installed two loopback devices, both with the same card, 4 in my example above.

Spawn a Direwolf instance for each (I use tmux or screen to give each a terminal
window) You can point a BPQ instance to one of these, and the BBS to the other.

Here is an example session using a setup like this, hence the fake BBS callsign:

```
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
u:jacob
p:

BPQ Telnet Server
'?' for Help>
C 1 HECTOR-3
HECTOR-7} Connected to HECTOR-3
Hello! Welcome to this very experimental packet BBS.

Try HELP

help
HELP This command.
FLIP Flip a coin.
BYE Peace!

flip
heads!

bye
*** Disconnected from Stream 1
Disconnected from Node - Telnet Session kept
```

Output from the BBS:

```
INFO:agw:connected
INFO:connecting...
INFO:Ready()
INFO:listening on HECTOR-3
INFO:SessionOpened(source='KN6YLK', destination='HECTOR-3')
INFO:SessionData(source='KN6YLK', destination='HECTOR-3', data=b'help\r')
INFO:[Help()]
INFO:[Send(text='HELP This command.\nFLIP Flip a coin.\nBYE Peace!\n')]
INFO:SessionData(source='KN6YLK', destination='HECTOR-3', data=b'flip\r')
INFO:[Flip()]
INFO:[Send(text='heads!\n')]
INFO:SessionData(source='KN6YLK', destination='HECTOR-3', data=b'bye\r')
INFO:[Bye()]
INFO:[Send(text='73\n'), Close()]
INFO:SessionClosed(source='KN6YLK', destination='HECTOR-3')
```

# What should I know about contributing?

I'm a big fan of `mypy` for type hints and static checking, as well as `black` for code formatting. There's also a minimal test suite brewing.
