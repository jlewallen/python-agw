#!env/bin/python3

import asyncio
import argparse
import sessions
import agw
import logging
import bbs

logger = logging.getLogger("bbs")


async def main():
    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)

    parser = argparse.ArgumentParser(prog="py-agw-bbs")
    parser.add_argument("--agw-server", required=True)
    parser.add_argument("--my-call", required=True)
    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    on_connected = loop.create_future()
    on_disconnected = loop.create_future()
    host, port = args.agw_server.split(":")
    events: asyncio.Queue[sessions.Event] = asyncio.Queue()
    transport, protocol = await loop.create_connection(
        lambda: agw.AgwClientProtocol(on_connected, on_disconnected, events),
        host,
        int(port),
    )

    behavior = bbs.SimpleBbs(
        call=args.my_call,
        protocol=protocol,
        welcome="Hello! Welcome to this very experimental packet BBS.\r\rTry HELP\r\r",
    )

    try:
        logging.info("connecting...")
        await on_connected

        protocol.ask_ports()

        while True:
            event = await events.get()
            logging.info(f"{event}")
            event.visit(behavior)
    finally:
        print()
        behavior.shutdown()
        transport.close()


if __name__ == "__main__":
    asyncio.run(main())
