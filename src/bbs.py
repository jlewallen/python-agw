from typing import Dict, Optional, List, Callable, Type, Any

import dataclasses
import logging
import random
import string
import argparse
from sessions import (
    Visitor,
    Ready,
    SessionOpened,
    SessionData,
    SessionClosed,
    Response,
    Protocol,
)


@dataclasses.dataclass
class CommandResponse:
    def send(self, session: "UserSession", protocol: Protocol):
        pass


@dataclasses.dataclass
class Close(CommandResponse):
    def send(self, session: "UserSession", protocol: Protocol):
        protocol.bye(session.source, session.destination)


@dataclasses.dataclass
class Send(CommandResponse):
    text: str

    def send(self, session: "UserSession", protocol: Protocol):
        text = self.text.replace("\n", "\r") + "\r"
        protocol.send_data(session.source, session.destination, text.encode("utf-8"))


@dataclasses.dataclass
class CommandContext:
    parser: argparse.ArgumentParser
    command: "Command"
    args: Any  # TODO

    def execute(self) -> List[CommandResponse]:
        return self.command.execute(self)


@dataclasses.dataclass
class Command:
    @staticmethod
    def parser(parsers: argparse.Namespace):
        pass

    def execute(self, ctx: CommandContext) -> List[CommandResponse]:
        return []


@dataclasses.dataclass
class CommandParser:
    grammar: str
    factory: Callable

    def try_parse(self, line: str) -> Optional[Command]:
        if self.grammar == line:
            return self.factory()
        return None


@dataclasses.dataclass
class Help(Command):
    @staticmethod
    def parser(parsers: argparse.Namespace):
        parsers.add_parser("help", help="get help").set_defaults(command=Help)

    def execute(self, ctx: CommandContext) -> List[CommandResponse]:
        return [Send(ctx.parser.format_help())]


@dataclasses.dataclass
class Lorem(Command):
    @staticmethod
    def parser(parsers: argparse.Namespace):
        parser = parsers.add_parser("lorem", help="generate random data")
        parser.set_defaults(command=Lorem)
        parser.add_argument("bytes", type=int, default=8)

    def execute(self, ctx: CommandContext) -> List[CommandResponse]:
        gibberish = "".join(
            random.choices(
                string.ascii_uppercase + string.ascii_lowercase, k=ctx.args.bytes
            )
        )
        chunks = (gibberish[0 + i : 64 + i] for i in range(0, len(gibberish), 64))
        return [Send("\r".join(chunks))]


@dataclasses.dataclass
class Flip(Command):
    @staticmethod
    def parser(parsers: argparse.Namespace):
        parsers.add_parser("flip", help="flip a coin").set_defaults(command=Flip)

    def execute(self, ctx: CommandContext) -> List[CommandResponse]:
        return [Send(random.choice(["heads!\n", "tails!\n"]))]


@dataclasses.dataclass
class Bye(Command):
    @staticmethod
    def parser(parsers: argparse.Namespace):
        parsers.add_parser("bye", help="sign off, disconnecting").set_defaults(
            command=Bye
        )

    def execute(self, ctx: CommandContext) -> List[CommandResponse]:
        # TODO This doesn't work, AGW gets them both but closes before sending.
        return [Send("73\n"), Close()]


@dataclasses.dataclass
class Activity:
    def data(self, text: str):
        return []


class ErrorCatchingArgumentParser(argparse.ArgumentParser):
    def exit(self, status=0, message=None):
        pass


@dataclasses.dataclass
class Menu(Activity):
    commands: List[Type]
    rx: str = ""

    def parse(self, text: str) -> List[CommandContext]:
        parsed = []
        parsing = self.rx + text.lower()
        for line in parsing.splitlines(True):
            if line.endswith("\r"):
                line = line.strip()
                parser = ErrorCatchingArgumentParser(prog="menu")
                parsers = parser.add_subparsers(help="available commands")
                for command_type in self.commands:
                    command_type.parser(parsers)
                try:
                    args = parser.parse_args(line.split(" "))
                    logging.debug(f"args {args}")
                    if args and "command" in args:
                        parsed.append(
                            CommandContext(
                                parser=parser, command=args.command(), args=args
                            )
                        )
                except argparse.ArgumentError:
                    pass
            else:
                self.rx = line
        return parsed

    def data(self, text: str) -> List[CommandResponse]:
        commands = self.parse(text)
        logging.info(f"{commands}")
        responses = flatten([command.execute() for command in commands])
        logging.info(f"{responses}")
        return responses


@dataclasses.dataclass
class UserSession:
    source: str
    destination: str
    activity: Activity = Menu(commands=[Help, Lorem, Flip, Bye])


@dataclasses.dataclass
class SimpleBbs(Visitor):
    call: str
    protocol: Protocol
    welcome: str
    active: Dict[str, UserSession] = dataclasses.field(default_factory=dict)

    def ready(self, event: Ready) -> Optional[Response]:
        logging.info(f"listening on {self.call}")
        self.protocol.listen(self.call)
        return None

    def session(self, call: str) -> UserSession:
        if call not in self.active:
            self.active[call] = UserSession(source=self.call, destination=call)
        return self.active[call]

    def opened(self, event: SessionOpened) -> Optional[Response]:
        assert event.destination == self.call
        session = self.session(event.source)
        if self.welcome:
            self.protocol.send_data(
                session.source, session.destination, self.welcome.encode("utf-8")
            )
        return None

    def data(self, event: SessionData) -> Optional[Response]:
        assert event.destination == self.call
        session = self.session(event.source)
        for response in session.activity.data(event.data.decode("utf-8")):
            response.send(session, self.protocol)
        return None

    def closed(self, event: SessionClosed) -> Optional[Response]:
        assert event.source == self.call
        if event.destination in self.active:
            del self.active[event.destination]
        return None

    def shutdown(self):
        for session in self.active.values():
            self.protocol.bye(session.source, session.destination)


def flatten(l):
    return [item for sl in l for item in sl]
