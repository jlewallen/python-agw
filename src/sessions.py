from typing import Dict, List, Optional

import dataclasses
import logging


@dataclasses.dataclass
class Event:
    def visit(self, visitor: "Visitor"):
        pass


@dataclasses.dataclass
class Ready(Event):
    def visit(self, visitor: "Visitor"):
        return visitor.ready(self)


@dataclasses.dataclass
class SessionEvent(Event):
    source: str
    destination: str


@dataclasses.dataclass
class SessionOpened(SessionEvent):
    def visit(self, visitor: "Visitor"):
        return visitor.opened(self)


@dataclasses.dataclass
class SessionData(SessionEvent):
    data: bytes

    def visit(self, visitor: "Visitor"):
        return visitor.data(self)


@dataclasses.dataclass
class SessionClosed(SessionEvent):
    def data(self, visitor: "Visitor"):
        return visitor.closed(self)


class Response:
    pass


class Visitor:
    def ready(self, event: Ready) -> Optional[Response]:
        return None

    def opened(self, event: SessionOpened) -> Optional[Response]:
        return None

    def data(self, event: SessionData) -> Optional[Response]:
        return None

    def closed(self, event: SessionClosed) -> Optional[Response]:
        return None


class Protocol:
    def listen(self, call: str):
        pass

    def send_data(self, source: str, destination: str, data: bytes):
        pass

    def bye(self, source: str, destination: str):
        pass
