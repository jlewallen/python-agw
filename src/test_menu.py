from bbs import Help, Flip, Menu, Lorem


def get_menu():
    return Menu(commands=[Help, Flip, Lorem])


def test_menu_parse_empty():
    assert get_menu().parse("") == []


def test_menu_parse_no_newline():
    assert get_menu().parse("flip") == []


def test_menu_parse_help():
    assert isinstance(get_menu().parse("help\r")[0].command, Help)


def test_menu_parse_flip():
    assert isinstance(get_menu().parse("flip\r")[0].command, Flip)


def test_menu_parse_lorem():
    assert isinstance(get_menu().parse("lorem\r")[0].command, Lorem)


def test_menu_parse_lorem_bytes():
    assert isinstance(get_menu().parse("lorem 100\r")[0].command, Lorem)


def test_menu_parse_unknown():
    assert get_menu().parse("ok") == []


def test_menu_partial_parse():
    menu = get_menu()
    assert menu.parse("fli") == []
    assert isinstance(menu.parse("p\r")[0].command, Flip)
