from typing import Tuple, Dict, Optional

import asyncio
import dataclasses
import struct
import logging

import sessions

AGW_HEADER_LENGTH = 36
PID_NONE = b"\0"
AGW_KIND_REGISTER_CALLSIGN = b"X"
AGW_KIND_ASK_PORTS = b"G"
AGW_KIND_CONNECT = b"C"
AGW_KIND_DISCONNECT = b"d"
AGW_KIND_DATA = b"D"


@dataclasses.dataclass
class Port:
    number: int


@dataclasses.dataclass
class AgwHeader:
    port: Port
    kind: bytes
    source: str = ""
    destination: str = ""
    pid: bytes = PID_NONE
    length: int = 0


@dataclasses.dataclass
class AgwPacket:
    header: AgwHeader
    data: bytes = bytes()

    def to_bytes(self) -> bytearray:
        assert len(self.data) == self.header.length
        buf = bytearray(
            struct.pack(
                "<icccc10s10sii",
                self.header.port.number,
                self.header.kind,
                b"\0",
                self.header.pid,
                b"\0",
                self.header.source.encode("utf-8"),
                self.header.destination.encode("utf-8"),
                self.header.length,
                0,
            )
        )
        return buf + self.data

    @staticmethod
    def from_bytes(data: bytes) -> "AgwPacket":
        fields = struct.unpack("<icccc10s10sii", data[0:AGW_HEADER_LENGTH])
        header = AgwHeader(
            port=Port(number=fields[0]),
            kind=fields[1],
            pid=fields[3],
            source=fields[5].split(b"\0")[0].decode("utf-8"),
            destination=fields[6].split(b"\0")[0].decode("utf-8"),
            length=fields[7],
        )
        return AgwPacket(header=header, data=data[AGW_HEADER_LENGTH:])


@dataclasses.dataclass
class AgwClientProtocol(asyncio.Protocol, sessions.Protocol):
    on_connected: asyncio.Future[bool]
    on_disconnected: asyncio.Future[bool]
    events: asyncio.Queue
    transport: Optional[asyncio.Transport] = None

    def write(self, data: bytes):
        if self.transport:
            self.transport.write(data)

    def connection_made(self, transport):
        logging.info("agw:connected")
        self.transport = transport
        if not self.on_connected.cancelled():
            self.on_connected.set_result(True)
        self.events.put_nowait(sessions.Ready())

    def ask_ports(self):
        data = AgwPacket(
            header=AgwHeader(port=Port(number=0), kind=AGW_KIND_ASK_PORTS),
        ).to_bytes()
        self.write(data)

    def register(self, callsign: str, port: Port = Port(number=0)):
        data = AgwPacket(
            header=AgwHeader(
                port=port, kind=AGW_KIND_REGISTER_CALLSIGN, source=callsign
            ),
        ).to_bytes()
        self.write(data)

    # The station originating the connection (CallFrom) must had been previously
    # registered with AGWPE (‘X’ frame) for the connection to be successfully
    # established.
    def connect(self, source: str, destination: str, port: Port = Port(number=0)):
        data = AgwPacket(
            header=AgwHeader(
                port=port, kind=AGW_KIND_CONNECT, source=source, destination=destination
            ),
        ).to_bytes()
        self.write(data)

    def data(
        self, source: str, destination: str, data: bytes, port: Port = Port(number=0)
    ):
        data = AgwPacket(
            header=AgwHeader(
                port=port,
                kind=AGW_KIND_DATA,
                source=source,
                destination=destination,
                length=len(data),
            ),
            data=data,
        ).to_bytes()
        self.write(data)

    def disconnect(self, source: str, destination: str, port: Port = Port(number=0)):
        data = AgwPacket(
            header=AgwHeader(
                port=port,
                kind=AGW_KIND_DISCONNECT,
                source=source,
                destination=destination,
            ),
        ).to_bytes()
        self.write(data)

    def data_received(self, data: bytes):
        packet = AgwPacket.from_bytes(data)
        # logging.debug(f"agw:received: {packet}")
        if packet.header.kind == AGW_KIND_CONNECT:
            self.events.put_nowait(
                sessions.SessionOpened(
                    source=packet.header.source,
                    destination=packet.header.destination,
                )
            )
        if packet.header.kind == AGW_KIND_DATA:
            self.events.put_nowait(
                sessions.SessionData(
                    source=packet.header.source,
                    destination=packet.header.destination,
                    data=packet.data,
                )
            )
        if packet.header.kind == AGW_KIND_DISCONNECT:
            self.events.put_nowait(
                sessions.SessionClosed(
                    source=packet.header.source,
                    destination=packet.header.destination,
                )
            )

    def connection_lost(self, error):
        if not self.on_disconnected.cancelled():
            self.on_disconnected.set_result(True)
        super().connection_lost(error)

    def listen(self, call: str):
        self.register(call)

    def send_data(self, source: str, destination: str, data: bytes):
        self.data(source, destination, data)

    def bye(self, source: str, destination: str):
        self.disconnect(source, destination)


# eof
