#!/bin/bash

python3 -m venv env
echo starting venv $SHELL
. env/bin/activate
pip3 install -r requirements.txt
exec $SHELL -i
