checks: env
	env/bin/mypy src --ignore-missing-imports

freeze:
	pip3 freeze > requirements.txt

docs:
	sphinx-build docs _build

prettier: env
	python3 -m black src

env:
	echo please activate.sh venv

clean:
	rm -rf env
